# Python-Json-Flask-Login | Lastest Update <code>8 Aug 2021</code>


<h2>English</h2>
<h3>This project is an implementation of Flask, one of Python's web libraries and JSON.</h3>
<hr><br>
<h2>ภาษาไทย</h2>
<h3>โปรเจคนี้คือการนำ Flask ที่เป็นหนึ่งใน Web Library ของ Python มาประยุกต์ใช้กับ JSON โดยได้ใช้ JSON ในการเป็นตัวที่ใช้เก็บข้อมูล</h3><hr><br><br>
<h2>How to use<h2><br>

```
set FLASK_APP=main.py
```
```
set FLASK_ENV=development
```
```
flask run
```
  
<h2>Requirement</h2><br>

```
pip install Flask
```

 <br>
<h2>Updating Log</h2>
  
* 5 Aug 2021
  * Login page
  * JSON
* 6 Aug 2021
  * Profile Info Page
  * REST API
    * username
    * score
* 8 Aug 2021
  * Profile Picture Upload Page 
  
  <br>
<h2>Picture</h2><hr><br>
<code>/signin</code>
<img src="https://scontent.fbkk21-1.fna.fbcdn.net/v/t1.15752-9/229313522_550775872638200_5466831548473750642_n.png?_nc_cat=107&ccb=1-3&_nc_sid=ae9488&_nc_ohc=5nQzxcOw_zoAX9l7GBw&_nc_ht=scontent.fbkk21-1.fna&oh=3f5f8d7cdba5e9a2ed1d341c9e4ad5a5&oe=61329EA3"><br>
<code>/signup</code>
<img src="https://scontent.fbkk21-1.fna.fbcdn.net/v/t1.15752-9/232702341_300958265116348_2442791509478958799_n.png?_nc_cat=101&ccb=1-3&_nc_sid=ae9488&_nc_ohc=i4MiSSyNVesAX_61yOV&_nc_ht=scontent.fbkk21-1.fna&oh=5e5a73555f9daaaaed2dcd604d6bd7c3&oe=61316F8C">
<code>/userinfo/</code>
<img src="https://scontent.fbkk6-2.fna.fbcdn.net/v/t1.15752-9/228132395_234166941909507_7636189233259409638_n.png?_nc_cat=101&ccb=1-4&_nc_sid=ae9488&_nc_ohc=mj9jgDnGXQwAX99lhyY&tn=HsRQLDQgL073lxPm&_nc_ht=scontent.fbkk6-2.fna&oh=692edc05afe793c802ac926a5cbc2469&oe=6131E312">
  <code>/api/</code>
<img src="https://scontent.fbkk21-1.fna.fbcdn.net/v/t1.15752-9/222640290_950336555817980_1577264661621055369_n.png?_nc_cat=110&ccb=1-4&_nc_sid=ae9488&_nc_ohc=RhIiZ_B69KIAX9JyhC5&_nc_ht=scontent.fbkk21-1.fna&oh=bb0df0cec80ac72407f1671b4ca4aa18&oe=61325324">
  <code>/userinfo/upload</code>
  <img src="https://scontent.fbkk21-1.fna.fbcdn.net/v/t1.15752-9/234227195_262638382019340_308352488455880468_n.png?_nc_cat=101&ccb=1-4&_nc_sid=ae9488&_nc_eui2=AeFQ5YyIaswOo0hB7wDhxOWU18c762CiWEPXxzvrYKJYQxQ95DQBYFfdj6VQbb5nH3p31h92H6uuVjfpsfmdRZvc&_nc_ohc=r7owaju3s30AX_i3YG6&_nc_ht=scontent.fbkk21-1.fna&oh=099422425ddac19321d3b12da3947dd5&oe=61349B2A">
  
  

  <hr><br>
  <!-- <h3 align="center">Development By <a href="https://github.com/Karibura-Cyber">Karibura (Meck)</a> 100% No Copyright</h3> -->
